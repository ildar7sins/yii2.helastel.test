<?php

namespace api\modules\v1\controllers;

use Yii;
use yii\rest\Serializer;
use yii\rest\ActiveController;
use api\modules\v1\resources\User;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;

class AuthController extends ActiveController {
	
	/**
	 * @var string
	 */
	public $modelClass = User::class;
	
	/**
	 * @var array
	 */
	public $serializer = [
		'class' => Serializer::class,
		'collectionEnvelope' => 'items'
	];
	
	public function behaviors() {
		$behaviors = parent::behaviors();
		
		// add CORS filter
		$behaviors['corsFilter'] = [
			'class' => \yii\filters\Cors::className(),
			'cors' => [
				'Origin' => ['*'],
				'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
				'Access-Control-Allow-Credentials' => false,
				'Access-Control-Request-Headers' => ['*'],
				'Access-Control-Allow-Origin:' => ['*'],
			],
		];
		
		return $behaviors;
	}
	
	/**
	 * @throws BadRequestHttpException
	 * @throws NotFoundHttpException
	 */
	public function actionLogin() {
		$data = Yii::$app->query->getData();
		
		if (!isset($data['email']) || !$data['email']) {
			throw new BadRequestHttpException('Необходимо указать E-mail');
		}
		
		if (!isset($data['password']) || !$data['password']) {
			throw new BadRequestHttpException('Необходимо указать пароль');
		}
		
		$model = $this->findByEmail($data['email']);
		
		if ($model->validatePassword($data['password'])) {
			return $model;
		}
		
		throw new BadRequestHttpException('Неверная пара email/пароль');
	}
	
	/**
	 * @param $email
	 * @return \common\models\User|null
	 * @throws NotFoundHttpException
	 */
	protected function findByEmail($email) {
		if (is_null($model = User::findByEmail($email))) {
			throw new NotFoundHttpException('Пользователь не найден');
		}
		
		return $model;
	}
}
