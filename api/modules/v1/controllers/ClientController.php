<?php

namespace api\modules\v1\controllers;

use Yii;
use yii\rest\Controller;
use common\models\Client;
use yii\web\HttpException;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\HttpHeaderAuth;
use yii\filters\auth\QueryParamAuth;
use api\modules\v1\resources\Client as ClientResource;

/**
 * @author Eugene Terentev <eugene@terentev.net>
 */
class ClientController extends Controller {
	/**
	 * @return array
	 */
	public function behaviors() {
		$behaviors = parent::behaviors();
		
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::class,
			'authMethods' => [
				HttpBasicAuth::class,
				HttpBearerAuth::class,
				HttpHeaderAuth::class,
				QueryParamAuth::class
			]
		];
		
		return $behaviors;
	}
	
	/**
	 * @param $type
	 * @return string
	 * @throws HttpException
	 */
	public function actionCreate($type) {
		$model = new Client();
		$data = Yii::$app->query->getData();
		
		if ($model->load($data, '') && $model->create($type)) {
			return 'OK';
		}
		
		throw new HttpException(400, 'Не удалось создать, обратитесь к админестратору');
	}
	
	/**
	 * @param $type
	 * @return array
	 * @throws HttpException
	 */
	public function actionIndex($type) {
		return ClientResource::getAll($type);
	}
}
