<?php

namespace api\modules\v1\resources;


use yii\web\HttpException;
use common\models\ClientRedis;

class Client extends \common\models\Client {
	public function fields() {
		return [
			'id',
			'name',
			'email',
			'phone',
		];
	}
	
	/**
	 * @param string $type
	 * @return array
	 * @throws HttpException
	 */
	public static function getAll(string $type): array {
		if ($type === self::TYPE_MYSQL) {
			return self::find()->all();
		} else if ($type === self::TYPE_JSON) {
			return self::getJSON();
		} else if ($type === self::TYPE_XLSX) {
			return self::getXLSX();
		} else if ($type === self::TYPE_REDIS) {
			return ClientRedis::find()->all();
		} else {
			throw new HttpException(400, 'Неподдерживаемый тип данных');
		}
	}
}
