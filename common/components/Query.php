<?php

namespace common\components;

use Yii;
use yii\base\Component;

class Query extends Component {
	/**
	 * Get data from frontend request body/post
	 * @param bool $field
	 * @return array|mixed
	 * @throws \JsonException
	 */
	public function getData($field = false) {
		$data = empty((
		$body = json_decode(Yii::$app->request->getRawBody(), true)))
			? Yii::$app->request->post()
			: $body;
		if ($field) {
			return $data[$field];
		}
		return $data;
	}
}
