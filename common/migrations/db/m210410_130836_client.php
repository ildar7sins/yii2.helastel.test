<?php

use yii\db\Migration;

/**
 * Class m210410_130836_client
 */
class m210410_130836_client extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$this->createTable('client', [
			'id' => $this->primaryKey(11),
			'name' => $this->string(255)->unique()->notNull(),
			'email' => $this->string(255)->notNull(),
			'phone' => $this->bigInteger()->notNull(),
		]);
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		$this->dropTable('client');
	}
}
