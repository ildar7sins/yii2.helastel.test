<?php

namespace common\models;

use Yii;
use yii\helpers\Json;
use yii\web\HttpException;

/**
 * This is the model class for table "client".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property int $phone
 */
class Client extends \yii\db\ActiveRecord {
	const TYPE_MYSQL = 'mysql';
	const TYPE_JSON = 'json';
	const TYPE_XLSX = 'xlsx';
	const TYPE_REDIS = 'redis';
	
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'client';
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[['phone'], 'integer'],
			[['name', 'email', 'phone'], 'required'],
			[['name', 'email'], 'string', 'max' => 255],
		];
	}
	
	/**
	 * Update parent func...
	 * @param array $data
	 * @param null $formName
	 * @return bool
	 */
	public function load($data, $formName = null) {
		if (isset($data['phone'])) {
			$data['phone'] = (int)preg_replace('/[^0-9]/', '', $data['phone']);
		}
		
		return parent::load($data, $formName);
	}
	
	
	public function create($type) {
		if ($this->validate()) {
			if ($type === self::TYPE_MYSQL) {
				return $this->saveSql();
			} else if ($type === self::TYPE_JSON) {
				return $this->saveJSON();
			} else if ($type === self::TYPE_XLSX) {
				return $this->saveXLSX();
			} else if ($type === self::TYPE_REDIS) {
				return $this->saveRedis();
			} else {
				throw new HttpException(400, 'Неподдерживаемый тип данных');
			}
			
			return true;
		}
		
		return false;
	}
	
	/**
	 * @return bool
	 * @throws HttpException
	 */
	protected function saveSql() {
		if ((int)self::find()->where(['name' => $this->name])->count() === 0) {
			return $this->save();
		}
		
		throw new HttpException(400, 'Данный клиент уже зарегестрирован');
	}
	
	/**
	 * @return false|int
	 * @throws HttpException
	 */
	protected function saveJSON() {
		$file = Yii::getAlias('@base/common/data/client/data.json');
		
		if (!file_exists($file)) {
			file_put_contents($file, '');
		}
		
		$fileContent = file_get_contents($file);
		
		if ($fileContent) {
			$data = Json::decode($fileContent);
			if (!isset($data[str_replace(' ', '_', $this->name)])) {
				$data[str_replace(' ', '_', $this->name)] = [
					'name' => $this->name,
					'phone' => $this->phone,
					'email' => $this->email,
				];
			} else {
				throw new HttpException(400, 'Данный клиент уже зарегестрирован');
			}
			
		} else {
			$data = [
				str_replace(' ', '_', $this->name) => [
					'name' => $this->name,
					'phone' => $this->phone,
					'email' => $this->email,
				]
			];
		}
		
		return file_put_contents($file, Json::encode($data));
	}
	
	/**
	 * @return array
	 */
	public static function getJSON(): array {
		$data = [];
		$file = Yii::getAlias('@base/common/data/client/data.json');
		if (file_exists($file)) {
			$fileContent = file_get_contents($file);
			
			if ($fileContent) {
				$data = array_values(Json::decode($fileContent));
			}
		}
		
		return $data;
	}
	
	/**
	 * @return bool
	 * @throws HttpException
	 */
	protected function saveRedis() {
		$key = str_replace(' ', '_', $this->name);
		$model = ClientRedis::find()->where(['key' => $key])->one();
		
		if (is_null($model)) {
			$customer = new ClientRedis();
			$customer->key = str_replace(' ', '_', $this->name);
			$customer->name = $this->name;
			$customer->email = $this->email;
			$customer->phone = $this->phone;
			
			return $customer->save();
		} else {
			throw new HttpException(400, 'Данный клиент уже зарегестрирован');
		}
	}
	
	/**
	 * @return bool
	 */
	protected function saveXlsx() {
		$file = Yii::getAlias('@base/common/data/client/data.xlsx');
		
		try {
			if (!file_exists($file)) {
				$xlsx = \SimpleXLSXGen::fromArray([[$this->name, $this->email, $this->phone]]);
				$xlsx->saveAs($file);
			} else {
				$parse = \SimpleXLSX::parse($file);
				if ($parse) {
					$rows = $parse->rows();
					$key = array_search($this->name, array_column($rows, 0));
					
					if ($key === false) {
						array_push($rows, [$this->name, $this->email, $this->phone]);
						$xlsx = \SimpleXLSXGen::fromArray($rows);
						$xlsx->saveAs($file);
					} else {
						throw new HttpException(400, 'Данный клиент уже зарегестрирован');
					}
				}
			}
		} catch (\Exception $e) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * @return array
	 */
	public static function getXLSX(): array {
		$data = [];
		$file = Yii::getAlias('@base/common/data/client/data.xlsx');
		if (file_exists($file)) {
			$parse = \SimpleXLSX::parse($file);
			if ($parse) {
				$data = array_map(static function ($item) {
					return ['name' => $item[0], 'email' => $item[1], 'phone' => $item[2]];
				}, $parse->rows());
			}
		}
		
		return $data;
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id' => 'ID',
			'name' => 'Имя',
			'email' => 'Email',
			'phone' => 'Телефон',
		];
	}
}
