<?php

namespace common\models;

class ClientRedis extends \yii\redis\ActiveRecord {
	public static function primaryKey() {
		return ['key'];
	}
	
	/**
	 * @return array the list of attributes for this record
	 */
	public function attributes() {
		return ['key', 'name', 'email', 'phone'];
	}
	
}
